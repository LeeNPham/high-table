# API Endpoint Template
### «Human-readable of the endpoint»
##### Mandatory fields are:
```
* Endpoint path
* Endpoint method
* Response
* Response shape
* If your endpoint needs to know who the person is, then include the Headers/Authorization part.
* If your endpoint is a POST, PUT, or PATCH, include the Request shape (JSON) part.
```

* Endpoint path: «path to use»
* Endpoint method: «HTTP method»
* Query parameters:
  * «name»: «purpose»

* Headers:
  * Authorization: Bearer token

* Request shape (JSON):
    ```json
    «JSON-looking thing that has the
    keys and types in it»
    ```

* Response: «Human-readable description
            of response»
* Response shape (JSON):
    ```json
    «JSON-looking thing that has the
    keys and types in it»
    ```

### Get a random Yelp Restaurant
* Endpoint path: /random
* Endpoint method: GET
* Query parameters:
  * <<location>>:<<food>>
* Headers:
  * Authorization: YELP API Token
* Request shape (JSON):
    ```json
    { food: <food here>
      location: <location here>}
    ```
* Response: A page with picture / Restaurant name / location / phone / rating
* Response shape (JSON):
    ```json
    "businesses": [
		{
			"id": "zyDGcWX4CNlX9Jm6SEs37Q",
			"alias": "sizzling-lunch-fremont",
			"name": "Sizzling Lunch",
			"image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/hZo6f80heTi0d01BHjpYqw/o.jpg",
			"is_closed": false,
			"url": "https://www.yelp.com/biz/sizzling-lunch-fremont?adjust_creative=VoBbubkcXPTeU_FU6VavrA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VoBbubkcXPTeU_FU6VavrA" }
    ```

### Reroll a Yelp Restaurant
* Endpoint path: /Restaurants
* Endpoint method: GET
* Headers:
  * Authorization: Bearer token
* Response: Another restaurant
* Request body:
    ```json
    {
        "restaurant_name": string,
        "restaurant_profile_url": string,
        "description": string,
        "image_url": string,
        "ratings": number,
        "rating": number
    }
    ```
* Response: An indication of success or failure
* Response shape:
    ```json
    {
      "success": boolean,
      "message": string
    }
    ```

### Log in
#### https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST
* Endpoint path: /token
* Endpoint method: POST
* Request shape (form):
  * username: string
  * password: string
* Response: Account information and a token
* Response shape (JSON):
    ```json
    {
      "account": {
        «key»: type»,
      },
      "token": string
    }
    ```

### Log out
* Endpoint path: /token
* Endpoint method: DELETE
* Headers:
  * Authorization: Bearer token
* Response: Always true
* Response shape (JSON):
    ```json
    true
    ```