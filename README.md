## Deprecated Project 


Module3 Project Gamma

Getting started
You have a project repository, now what? The next section lists all of the deliverables that are due at the end of the week. Below is some guidance for getting started on the tasks for this week.

Deliverables


 Wire-frame diagrams

 API documentation

 Project is deployed to Heroku/GitLab-pages

 GitLab issue board is setup and in use


Project layout
The layout of the project is just like all of the projects you did with docker-compose in module #2. You will create a directory in the root of the repository for each service that you add to your project just like those previous projects were setup.

Directories
Several directories have been added to your project. The directories docs and journals are places for you and your team-mates to, respectively, put any documentation about your project that you create and to put your project-journal entries. See the README file in each directory for more info.
The other directories, ghi and sample_service, are sample services, that you can start building off of or use as a reference point.
Inside of ghi is a minimal React app that has an "under construction" page. It is setup similarly to all of the other React projects that you have worked on.
Inside of sample_service is a minimal FastAPI application. "Where are all the files?" you might ask? Well, the main.py file is the whole thing, and go take look inside of it... There's not even much in there..., hmm? That is FastAPI, we'll learn more about it in the coming days. Can you figure out what this little web-application does even though you haven't learned about FastAPI yet?

Other files
The following project files have created as a minimal starting point. Please follow the guidance for each one for a most successful project.


docker-compose.yaml: there isn't much in here, just a really simple UI. Add services to this file as you did with previous projects in module #2.

.gitlab-ci.yml: This is your "ci/cd" file where you will configure
automated unit tests, code quality checks, and the building and deployment
of your production system. Currently, all it does is deploy an
"under construction" page to your production UI on GitLab. We will learn
much more about this file

.gitignore: don't keep track of things you don't need to like
node_modules, __pycache__, etc.


How to complete the initial deploy
There will be further guidance on completing the initial deployment, but it just consists of these steps:

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

initial git testing from elu-dev branch
setup Heroku account and app
setup 2 CI/CD variables in GitLab
push to main
```
Instructions for deploying the project template:
0) Create a heroku account and app
1) edit `.gitlab-ci.yml`
	1) PUBLIC_URL : this is the gitlab pages URL
	2) REACT_APP_API_HOST : this is the URL to the service on Heroku
2) Heroku Variables:
	1) CORS_HOST
3) GitLab Variables:
	- `HEROKU_API_KEY` : (protected and masked)
	- `HEROKU_FASTAPI_APP`: name of the service in heroku
	- `REACT_APP_API_HOST`: this is the URL to the service on Heroku
4) Push to main
```
